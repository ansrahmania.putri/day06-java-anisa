package Assignment1d6;

import Assignment1d6.MahasiswaModel;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileWriter;

public class Mahasiswa {

    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        int pilihan;

        ArrayList<MahasiswaModel> mhsList = new ArrayList<>();

        do {
            System.out.println("Menu");
            System.out.println("1.  Input Direktori");
            System.out.println("2.  Tulis Laporan ke TXT");
            System.out.println("3.  EXIT");
            System.out.println("Pilih Menu (1/2/3)");
            pilihan = input.nextInt();

            switch (pilihan) {
                case 1:

                    System.out.print("Masukkan direktori: ");
                    String dir = input.next();
                    System.out.print("Masukkan nama file: ");
                    String file = input.next();

                    String filePath = dir+file;

                    FileReader fr=new FileReader(filePath);
                    int i = 0;

                    String strAccu = "";
                    //Read per character
                    while((i=fr.read()) != -1) {
                        //System.out.print((char) i);
                        strAccu = strAccu + (char) i;
                    }

                    String strAccuParsed [] = strAccu.split("\n");

                    for(int j=0;j<strAccuParsed.length;j++){
                        System.out.println(strAccuParsed[j]);
                        if(j > 0)
                        {
                            String strAccuParsed2 [] = strAccuParsed[j].trim().split(",");

                            int idReader = Integer.parseInt(strAccuParsed2[0]);
                            String namaReader = strAccuParsed2[1];
                            int nilaiReader = Integer.parseInt(strAccuParsed2[2]);

                            MahasiswaModel s = new MahasiswaModel(idReader, namaReader, nilaiReader);
                            mhsList.add(s);
                        }
                    }
                    fr.close();
                    break;

                case 2:
                    System.out.print("Masukkan direktori: ");
                    String dir2 = input.next();
                    System.out.print("Masukkan nama file: ");
                    String file2 = input.next();

                    String filePath2 = dir2+file2;
                    //writer
                    try {
                        FileWriter fw = new FileWriter(filePath2);
                        for (MahasiswaModel el: mhsList) {
                            fw.write("\nID: "+ el.getId() + "\nNama: " + el.getNama() + "\nNilai: " + el.getNilai());
                        }
                        fw.close();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    System.out.println("Berhasil...");
                    break;
                default:
                    if (pilihan == 3) {
                        break;
                    } else {
                        System.out.println("Menu yang anda pilih tidak tersedia");
                    }
            }
        } while (pilihan != 3);

    }

}
