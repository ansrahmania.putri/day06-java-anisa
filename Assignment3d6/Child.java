package Assignment3d6;

public class Child extends Parent{

    public Child() {
    }

    public void car() {
        System.out.println("Child's Car");
    }

    public void ParentInfo() {
        System.out.println("Parent name : " + super.getName());
        System.out.println("Parent's money : $" + super.getMoney());
    }

    public void ChildInfo() {
        System.out.println("Name : " + (this.name = "Tom"));
        System.out.println("Money : " + (this.money = 200));
    }


}
