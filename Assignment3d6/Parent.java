package Assignment3d6;

public class Parent {
    String name = "Mr. USD";
    int money = 2000000;

    public Parent() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void home (){
        System.out.print("Parent's Home");
    }
    public void car() {
        System.out.println("Parent's Car");
    }

}
