package Assignment4d6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class StaffView1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        ArrayList<Staff1> staffList = new ArrayList<>();

        do {
            System.out.println("MENU");
            System.out.println("1. Buat Staff");
            System.out.println("1. Tambah Absensi");
            System.out.println("3. Tampilkan Laporan Staff");
            System.out.println("4. EXIT");
            int pilihan = input.nextInt();

            switch (pilihan) {
                case 1:
                    System.out.print("ID: ");
                    int id = input.nextInt();
                    System.out.print("Nama: ");
                    String nama = input.next();
                    System.out.print("Absensi: ");
                    int absensi = input.nextInt();
                    System.out.print("Jabatan: ");
                    String jabatan = input.next();

                    Staff1 staff = new Staff1(id, nama, absensi, jabatan);
                    staffList.add(staff);
                    break;

                case 2:
                    System.out.print("Input ID: ");
                    long absensiId = input.nextLong();

                    for (Staff1 s : staffList) {
                        if (s.id == absensiId) {
                            System.out.println("ID anda yaitu : " + s.id);
                            System.out.println("Nama anda : " + s.nama);
                            s.tambahAbsensi();
                            System.out.println("Absensi berhasil ditambahkan");
                            break;
                        }
                    }
                    break;
                case 3:
                    Collections.sort(staffList, new Staff1.SortById());

                    System.out.println("\nSorted by ID");

                    Iterator<Staff1> itr = staffList.iterator();
                    int i = 0;

                    System.out.printf("%-10s %-10s %-10s %-10s %n", "ID", "Nama", "Jabatan", "Absensi/hari");

                    while (itr.hasNext()) {
                        Staff1 daftarStaff = itr.next();
                        System.out.printf("%-10s %-10s %-10s %-10s %n", daftarStaff.getId(), daftarStaff.getNama(), daftarStaff.getJabatan(), daftarStaff.getAbsensi());
                    }
                    break;
                default:
                    if (pilihan==3) {
                        break;
                    } else {
                        System.out.println("Menu yang anda pilih tidak tersedia");
                    }
            }

        } while (true);
    }
}