package Assignment4d6;

public abstract class Worker1 {

    int id;
    String nama;
    int absensi;

    public Worker1(int id, String nama, int absensi) {
        this.id = id;
        this.nama = nama;
        this.absensi = absensi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    abstract void tambahAbsensi ();


}
