package Assignment4d6;

import java.util.Comparator;
public class Staff1 extends Worker1{

    String jabatan;

    public Staff1(int id, String nama, int absensi, String jabatan) {
        super(id, nama, absensi);
        this.jabatan = jabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
    public static class SortById implements Comparator<Staff1> {

        public int compare(Staff1 a, Staff1 b) {
            return (int) (a.id - b.id);
        }
    }

    @Override
    void tambahAbsensi() {
        int addAbsensi = absensi + 1;
    }




}
