package Assignment5d6;

import Assignment4d6.Staff1;

import java.util.Comparator;

public class Staff2 implements Worker2 {
    int id;
    String nama;
    int gaji;
    int absensi = 20;

    public Staff2(int id, String nama, int gaji) {
        this.id = id;
        this.nama = nama;
        this.gaji = gaji;
        this.absensi = absensi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    @Override
    public void AbsensiMethod() {
       this.absensi = absensi + 1;
    }

    public static class SortById implements Comparator<Staff2> {

        public int compare(Staff2 a, Staff2 b) {
            return (int) (a.id - b.id);
        }
    }


}
