package Assignment5d6;

import Assignment4d6.Staff1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class StaffView2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        ArrayList<Staff2> staffList = new ArrayList<>();

        do {
            System.out.println("MENU");
            System.out.println("1. Buat Staff");
            System.out.println("2. Absensi Staff");
            System.out.println("3. Laporan Absensi");
            System.out.println("4. EXIT");
            int pilihan = input.nextInt();

            switch (pilihan) {
                case 1:
                    System.out.print("ID: ");
                    int id = input.nextInt();
                    System.out.print("Nama: ");
                    String nama = input.next();
                    System.out.print("Gaji: ");
                    int gaji = input.nextInt();

                    Staff2 staff = new Staff2(id, nama, gaji);
                    staffList.add(staff);
                    break;

                case 2:
                    System.out.print("Input ID: ");
                    long absensiId = input.nextLong();

                    for (Staff2 s : staffList) {
                        if (s.id == absensiId) {
                            System.out.println("ID anda yaitu : " + s.id);
                            System.out.println("Nama anda : " + s.nama);
                            s.AbsensiMethod();
                            System.out.println("Absensi berhasil ditambahkan");
                            break;
                        }
                    } break;
                case 3:
                    Collections.sort(staffList, new Staff2.SortById());

                    System.out.println("\nSorted by ID");

                    Iterator<Staff2> itr = staffList.iterator();
                    int i = 0;

                    System.out.printf("%-10s %-10s %-10s %-10s %n", "ID", "Nama", "Jabatan", "Absensi/hari");

                    while (itr.hasNext()) {
                        Staff2 daftarStaff = itr.next();
                        System.out.printf("%-10s %-10s %-10s %-10s %n", daftarStaff.getId(), daftarStaff.getNama(), daftarStaff.getGaji(), daftarStaff.getAbsensi());
                    }
                    break;

                default:
                    if (pilihan==4) {
                        break;
                    } else {
                        System.out.println("Menu yang anda pilih tidak tersedia");
                    }
            }

        } while (true);
    }
}