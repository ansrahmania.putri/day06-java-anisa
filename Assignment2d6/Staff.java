package Assignment2d6;

import java.util.Comparator;

public class Staff extends Worker {
        String jabatan;

    public Staff(int id, String nama, String jabatan) {
        super(id, nama);
        this.jabatan = jabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public static class SortById implements Comparator<Staff> {

        public int compare(Staff a, Staff b) {
            return (int) (a.id - b.id);
        }
    }


}
