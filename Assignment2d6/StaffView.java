package Assignment2d6;

import Assignment2d6.Staff;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class StaffView {


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        ArrayList<Staff> staffList = new ArrayList<>();

        do {
            System.out.println("MENU");
            System.out.println("1. Buat Staff");
            System.out.println("2. Tampilkan Laporan Staff");
            System.out.println("3. EXIT");
            int pilihan = input.nextInt();

            switch (pilihan) {
                case 1:
                    System.out.print("ID: ");
                    int id = input.nextInt();
                    System.out.print("Nama: ");
                    String nama = input.next();
                    System.out.print("Jabatan: ");
                    String jabatan = input.next();

                    Staff staff = new Staff(id, nama, jabatan);
                    staffList.add(staff);
                    break;

                case 2:
                    Collections.sort(staffList, new Staff.SortById());

                    System.out.println("\nSorted by ID");

                    Iterator<Staff> itr = staffList.iterator();
                    int i = 0;

                    System.out.printf("%-10s %-10s %-10s %n", "ID", "Nama", "Jabatan");

                    while (itr.hasNext()) {
                        Staff daftarStaff = itr.next();
                        System.out.printf("%-10s %-10s %-10s %n", daftarStaff.getId(), daftarStaff.getNama(), daftarStaff.getJabatan());
                    }
                    break;

                default:
                    if (pilihan==3) {
                        break;
                    } else {
                        System.out.println("Menu tidak ada dipilihan");
                    }
            }
        } while (true);
    }
}
